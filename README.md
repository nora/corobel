# Corobel

Translate Cohost projects and posts into RSS 2.0 feeds so you can follow them in your RSS reader.

## What it currently does

Corobel provides feeds at `/<project>/feed.rss` and Webfinger objects at `.well-known/webfinger?<project>`.

Set your domain name with `--domain`. If you must, you can configure the base URL with `--base-url`.
A template SystemD unit file is provided at `config/corobel.service`. See `Rocket.toml` for the appropriate
ports to use for development and deployment.

## Todo

- [x] Webfinger for Cohost projects
    - [ ] Handle redirects
- [x] RSS feeds for projects
- [x] Index page explaining what's going on
- [x] Better support for transparent shares
    - [x] Add feed without shares
- [ ] More robust parsing (defaults for all!)
- [ ] RSS feeds for tags
- [x] Atom Extension pagination support
- [x] Disable pagination
    - [x] HTTP Cacheing
    - [x] Data cacheing
- [x] Nicer theme
- [ ] Read More support
- [ ] Dublin Core support
- [x] Media Envelope support
