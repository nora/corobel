use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct CohostWebfingerResource {
    subject: String,
    aliases: Vec<String>,
    links: Vec<CohostWebfingerProfileLink>,
}

#[derive(Debug, Serialize)]
pub struct CohostWebfingerProfileLink {
    rel: String,
    #[serde(rename = "type")]
    t_type: String,
    href: String,
}

impl CohostWebfingerResource {
    pub fn new<S: AsRef<str>, T: AsRef<str>>(
        project_id: S,
        domain: T,
        base_url: impl AsRef<str>,
    ) -> Self {
        let mut corobel_link = CohostWebfingerProfileLink::new(project_id.as_ref().clone());
        corobel_link.href = format!(
            "https://{}{}{}/feed.rss",
            domain.as_ref().clone(),
            base_url.as_ref().clone(),
            project_id.as_ref().clone()
        );
        corobel_link.rel = "feed".into();
        corobel_link.t_type = "application+rss/xml".into();
        Self {
            subject: format!("acct:{}@{}", project_id.as_ref(), domain.as_ref()),
            aliases: vec![format!("acct:{}@cohost.org", project_id.as_ref())],
            links: vec![CohostWebfingerProfileLink::new(project_id), corobel_link],
        }
    }
}

impl CohostWebfingerProfileLink {
    pub fn new<S: AsRef<str>>(project_id: S) -> Self {
        Self {
            rel: "http://webfinger.net/rel/profile-page".into(),
            t_type: "text/html".into(),
            href: format!("https://cohost.org/{}", project_id.as_ref()),
        }
    }
}
